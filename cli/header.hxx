// file      : cli/header.hxx
// author    : Boris Kolpackov <boris@codesynthesis.com>
// copyright : Copyright (c) 2009-2019 Code Synthesis Tools CC
// license   : MIT; see accompanying LICENSE file

#ifndef CLI_HEADER_HXX
#define CLI_HEADER_HXX

#include <cli/context.hxx>

void
generate_header (context&);

#endif // CLI_HEADER_HXX
