// file      : cli/man.hxx
// author    : Boris Kolpackov <boris@codesynthesis.com>
// copyright : Copyright (c) 2009-2019 Code Synthesis Tools CC
// license   : MIT; see accompanying LICENSE file

#ifndef CLI_MAN_HXX
#define CLI_MAN_HXX

#include <cli/context.hxx>

void
generate_man (context&);

#endif // CLI_MAN_HXX
